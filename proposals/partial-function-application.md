```Somnium

func add(x: int, y: int) -> int { x + y }

func main(args: [string]) {
  let addTwo = add(2) // \y -> 2 + y
  let result = addTwo(10) // 12
}

```