# Advanced functional for loop

```Somnium

let items = [1, 2, 3, 4]

// Map all values in items with the statements in the for block.
let mapped: [string] = for i in items { yield i as string }

// 
let filtered: [int] = for i in items { if i % 2 == 0 { yield i } }

let reduced: int = for i in items yield n = 0 { n += i } // last non-void value produced is result of the for expr


// yield can be used to generate more than 1 item per cycle
let generated = for i in items {
  yield i * i
  yield i * i * i
}

// 
let containsTwo = for i in items yield result = false { if i == 2 { result := true; break } }


```