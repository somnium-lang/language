```Somnium

trait HavingFun {
  prop fun: bool {get}
}

class Foo with HavingFun {
  prop fun => true
}

class Bar with HavingFun {
  prop fun => false
}

// trait types are translated into union types
// so HavingFun => type HavingFun = Foo | Bar

func areWeHavingFun(f: HavingFun) {
  // f can be Foo or Bar, but since they both have a member 'fun'
  // we don't need to know the exact type
  if f.fun {
    println('yes')
  } else {
    println('no')
  }
}

```

Runtime the union types will have a vtable for dynamic dispatching.

```C

struct UnionType {
  void* vtable;
  int typeid;
};

```