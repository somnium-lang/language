# Memory management using ownership

Memory allocations in software need to be managed in some way in order to free memory that is no longer in use. While this is not necessary for short lived programs or software that simply doesn't allocate more than a fixed amount of memory, a lot of software needs to be able to allocate memory on demand. Currently the most popular ways of managing memory are:

- Garbage Collector
- Manual memory management
- RAII



## Ownership types

- Local; memory is owned by the stack
- Unique; heap allocation with a single owner
- Shared; heap allocation with one or more owners
- Weak; reference to either a unique or shared allocation
- Managed; heap allocation in a managed memory pool (garbage collected)
- Remote (unique/shared/weak); reference to a unique/shared/weak allocation/reference in another thread/location

```Somnium

struct Foo {}

let foo = Foo {} // stack allocated (local) instance of Foo
let foo = local Foo {} // same as above
let foo = shared Foo {} // shared ownership
let foo = unique Foo {} // single owner

// references can only be stored if the reference does not outlive foo
let r = ref foo

let w = weak foo // weak reference to foo, does not claim ownership (valid for unique and shared instances)


class Bar {}

let bar = Bar {}
let bar = local Bar {} // stack allocated (local) instance of Bar
let bar = shared Bar {} // shared ownership
let bar = unique Bar {} // single owner, this is the default for classes and thus the same as the first one

// ref Bar is a direct reference to an instance of Bar, not a reference to an unique pointer
// the default ownership specifier is ignored in this case
func function(b: ref Bar) 
// in order to pass a reference to a unique pointer, fully specify the type
func function(b: ref unique Bar)

// the default ownership type can be set for a specific class by adding the specifier to the declaration
shared class ASharedClass {}

// the type of 'x' is equal to 'shared ASharedClass'
func function(x: ASharedClass)


// traits default to unique, same as classes and support both shared and unique ownership

// managed ownership
class Tree with OwnerOf<Leaf> {}
class Leaf ownedby Tree {}
```