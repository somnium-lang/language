```Somnium

class Matchable {
  value: int | string

  alloc(this.value) {}

  func printType() {
    let t = match value {
      _: int -> 'int'
      _: string -> 'string'
    }

    println("type: {t}")
  }

  operator match() -> opt int {
    match value {
      v: int -> v
      _ -> null
    }
  }
  operator match() -> opt string {
    match value {
      v: string -> v
      _ -> null
    }
  }
}

// ---

let m = Matchable(2)
let i = match m {
  v: int -> v
  v: string -> int::fromString(v)
}



```