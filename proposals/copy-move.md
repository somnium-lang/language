```Somnium

class Object {
  // presence of non-copyable object makes the class move only
  // (class instances are unique by default and thus move only)
  x: Object

  alloc (this.x) {}
}

@no-copy
struct Data {
  // raw pointers can be copied
  // but here we rather move it
  p: ptr [byte]

  // move allocator
  move old {
    p := old.p
    old.p := 0
  }

}


```