```Somnium
const import std::file // const imports are only available at compile time
const LANGUAGE = 'en'

// non-literals cannot be used in non const functions
// unless the value can be converted to a literal at compile time

const trTable = buildTable()

const func buildTable() -> [string => string] {
  let f = File::open("./strings/{LANGUAGE}.txt")

  // parse file and return result
}

const func tr(s: string) -> string {
  trTable[s]
}

func main(args: [string]) {
  println(tr('HELLO_WORLD'))
}


```

Maybe use different identifiers?


## Annotations

```Somnium
import Somnium::Ast
import Somnium::Ast::Builders as AstBuilders

annotation @logged() {
  // 'this' is the AstNode that was annotated
  using function = this as FunctionDecl {
    let funcName = function.id as string
    let oldBlock = function.block.toSource()
    // 'native somnium' simply get turned into a string
    // but this way the code can be syntax highlighted 
    // and can potentially be checked for syntax errors
    // the '$' sign variables are use to replace the strings
    function.block.replaceWith(AstBuilders::buildBlock(native somnium {
      println('calling $funcName')
      $oldBlock
    }, AstBuilders::Scope::Block, ['funcName': funcName, 'oldBlock': oldBlock]))
    return
  }
}

@logged
func test() {
  println('test func')
}

func main(args: [string]) {
  test() // calling this will print: 'calling test' followed by 'test func'
}
```

## Conditional declarations

```Somnium

class Object {

  if isDebugBuild {
    func debugStuff() {
      //
    }
  }
}


class Foo<T> {
  // during compilation, the condition is evaluated with generic argument T exposed as a TypeInfo object.
  if T.name == 'int' {
    prop count: int {
      //
    }
  }
}

```