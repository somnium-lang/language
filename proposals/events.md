

```Somnium

class Foo {

  event onDidCalc(result: int)

  func calc(a: int, b: int) -> int {
    let return result = a * b
    onDidCalc(result)
  }
}

class Bar {

  alloc(foo: mut ref Foo) {
    foo.onDidCalc => printResult
  }

  func printResult(i: int) {
    println("i: {i}")
  }
}

func main(args: [string]) {
  let foo = Foo()
  let bar = Bar(mut ref foo)
  
  foo.calc(2, 6)
  // output: 'i: 12' via Bar::printResult
}

```