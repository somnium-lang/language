```Somnium

class External {
  func mut change(i: int) {
    //
  }
}

class Foo {
  e: External

  func foo() {
    e.change(2) // error, func foo cannot mutate state, change mutates state
  }
}

class Bar {
  /* keyword here */ e: External

  func bar() {
    e.change(3) // OK, e is marked as an external resource, mutability is not checked
  }
}

```