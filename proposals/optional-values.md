```Somnium

// optional types are annotated with the `opt` keyword

type Number = int | float
func stringToNumber(s: string) -> opt Number {
  // since the string is either a number or it is not, it's overkill to create error types for this
  // by using optionals it is also easier to work with the result
}

// null coalescing operator, will either use the result of the function call or 0 if the result was `null`
let i = stringToNumber('2') ?? 0

// we can also force the optional to a value
// this will throw an exception if the value was `null`
let i = !stringToNumber('2')

// a value can also be turned into an optional
let mut i = ?2

```