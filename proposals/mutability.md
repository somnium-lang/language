```Somnium

struct Vector {
  x: int
  y: int
}

let x = 2
x := 4 // error; x is immutable
let s = Vector {x: 2, y: 4}
s.x := 12 // error; s is immutable
let h = unique Vector { x: 2, y: 4 }
h.x := 12 // error; h is immutable

let mut x = 2
x := 4 // ok


func prod(v: unique Vector) -> int {
  v.x * v.y
}

```