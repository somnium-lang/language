# Scope guards

```Somnium

func function() {
  scope exit {
    // is called when existing the statement's scope
  }

  scope error {
    // called when an error or exception occurred
  }

}

```