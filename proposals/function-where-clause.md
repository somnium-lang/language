

```Somnium

func div(a: int, b: int) -> int
where b > 0 {
  a / b
}

func generic<T>(obj: T)
where T: Trait1 + Trait2 // T must be a type implementing both Trait1 and Trait2

func genericAdd<T>(a: T, b: T)
where T: int | float { // T matches the specified type (either int or float)
  a + b
}

```