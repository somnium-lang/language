```Somnium

async func routine() -> fut int {
  let i = await waitForNumber()

  i * 2
}

```


```Somnium
class Test {
  _state: int

  // non mutable scope async func will pass a copy of _state to ensure _state remains the same during the whole execution
  async func test(i :int) {
    let x = _state * i

    await task()

    doSomething(_state)
  }

  // mutable scope
  async func mut test2() {
    let x = _state

    await task()

    let y = _state

    if x != y {
      // something changed
    }
  }

}

```

```Somnium

// co-routine will cancel previous co-routines of this function
// resulting in a null value
// this is useful in cases where calling the function again makes
// the result of the previous call obsolete
protected async func foo() -> fut opt int {
  doWork()
  let i = await getNumber()

  i
}

async func bar() {
  let x = await [ foo(), foo() ]
  // first value in x will be null
}

```