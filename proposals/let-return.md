
```Somnium

func test(i: int) -> int {
  let return result = i * 2 // return 'result' when exiting the function scope
  doStuff()
}

func main(args: [string]) {
  let i = test(2)
  println("i: {i}") // prints: i: 4
}

```