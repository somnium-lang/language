```Somnium

class Mapper {
  values: [string => int] = []

  operator (member: string) -> int { values[member] }
}


// ----

func withMapper(m: ref Mapper) {
  m.someValue + m.otherValue
}

```