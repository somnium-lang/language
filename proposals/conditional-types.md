# Conditional types

```Somnium

// expr must return either a bool or T | null
type Month = i: uint => i <= 12

func setMonth(d: mut ref Date, m: Month) {
  d.month = m
}

func main(args: [string]) {
  let mut d = Date()
  setMonth(mut ref d, 11) // ok, literal '11' is less or equal to 12 which can be checked at compile time
  setMonth(mut ref d, 44) // compile error, literal is out of range

  let m = uint::parse(args[0])
  setMonth(mut ref d, m) // compile error, cannot convert uint to Month
  setMonth(mut ref d, Month(m)) // tries to convert the uint to a Month. returns the uint if <= 12 else throws an error

  catch e: InvalidTypeConversion {
    // m was not less or equal to 12
  }
}

type EmailAddress = s: string => isValidEmail(s)
// same as:
type EmailAddress = s: string => if isValidEmail(s) { s } else { null }
// its possible to return a string different from 's'

func sendEmail(a: EmailAddress) {
  let sender = MailSender()
  sender.to += a
  sender.title := 'test'
  sender.body := 'hello'
  sender.send()
}

func foo(form: FormData) {
  let a = EmailAddress(form['email']) ?? return
  sendEmail(a)
}

```