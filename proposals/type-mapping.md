```Somnium

// enum classes are special enums declaring types instead of values
// the WorkType type is a union type of all its child elements
enum class WorkType {
  Mining // type of element will be WorkType::Mining which has one possible value
  Smithing
}

type TypeMap {
  WorkType::Mining => MiningResult
  WorkType::Smithing => SmithingResult
}

func work<T>(type: T, time: int) -> TypeMap[T]
where T: WorkType {
  match type {
    _: WorkType::Mining -> MiningResult(time)
    _: WorkType::Smithing -> SmithingResult(time)
  }
}

```