```Somnium

trait HavingHello {
  func hello()
}

class ObjectA with HavingHello {
  func hello() {
    println('hello a')
  }
}

class ObjectB with HavingHello {
  func hello() {
    println('hello b')
  }
}

struct Structure with HavingHello {
  func hello() {
    println('hello struct')
  }
}


// classes default to unique ownership and structs to local
// traits default to unique as well

func withHavingHello(whh: HavingHello) {
  whh.hello()
}

func test() {
  let a = ObjectA{} // let a: unique ObjectA
  let b = ObjectB{}
  let s = Structure{}

  withHavingHello(move a) // must move a since there can be only one instance of ObjectA
  withHavingHello(move b) // same as above for ObjectB
  withHavingHello(move s) // error: HavingHello type assumes a unique instance
  withHavingHello(s) // error: HavingHello type assumes a unique instance
}


// we can make a trait default to shared

shared trait Foo {}

// local traits are not supported for now


```