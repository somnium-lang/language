```Somnium

func test(t: int) -> int {
  let x = #xs {
    if t > 2 {
      return #xs t
    }

    let z = getNr()
    z + z * t
  }

  if x > 100 {
    return x
  }

  x * x
}

func test(t: int) -> int {
  let x = {
    if t > 2 {
      t
    } else {
      let z = getNr()
      z + z * t
    }
  }

  if x > 100 {
    x
  } else {
    x * x
  }
}

```