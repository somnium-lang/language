```Somnium

class UseOnce with Usable<int> {
  i: int
  valid = true

  alloc(this.i) {}

  func onStartUsing() -> opt int {
    if !valid { null } else { i }
  }

  func onStopUsing() {
    valid := false
  }
}


func main(args: [string]) {
  let x = UseOnce(2)

  using x {
    // x.onStartUsing() returned 2
    // x is now 2
    println("x == {x}") // prints: x == 2
    // when exiting this scope x.onStopUsing() is called
  }

  using x {
    // x should be invalidated and thus not enter this block
  } else {
    // x.onStartUsing() returned null
    println('x was null the second time')
  }

  // we can test and use multiple values at once
  // as well as create bindings

  let y = UseOnce(3)

  using x, y, z = getUsable() {
    // if x, y and z are valid go here
  } else {
    // if any of the tested values was null, go here
  }

  let y = using y { y } else { 0 } // we can use it as an expression
}


```