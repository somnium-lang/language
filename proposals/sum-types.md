```Somnium

trait HavingA {
  func a()
}

trait HavingB {
  func b()
}

type AandB = HavingA + HavingB // a type that implements both HavingA and HavingB
type AorB = HavingA | HavingB // a type that is either HavingA, HavingA=B or both

```