```Somnium

class Element {
  alloc() {}
  func hello() {
    println('hello');
  }
}

proxy Slot for Element {
  element: Element

  alloc(this.element) {}

  func inProxy() {
    println('in proxy')
  }

  operator proxy() -> Element { element }
}


let s = Slot(Element())

s.hello() // calls hello() in Element
s.inProxy() // calls inProxy() in Slot


```