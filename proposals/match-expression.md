```Somnium

// type matching
func double(i: int | string) -> int
throws InvalidNumber {
  match i {
    i: int -> i * 2
    s: string -> using i = s as string { i * 2 } else { throw InvalidNumber }
  }
}

// value matching
match value {
  1 -> 'one'
  2 -> 'two'
  _ -> 'other'
}

// match enum cases
enum Result<T> {
    Ok(value: T),
    Err(e: string)
}

func withResult(r: Result<int>) {
    let i = match r {
        Ok(v) -> v
        Err(e) -> -1
    }
}

```