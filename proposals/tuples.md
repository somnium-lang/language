```Somnium

func retTuple() -> (int, bool) {
  (2, true)
}


let t = retTuple()
let a: int = t.0
let b: bool = t.1

let (a, b) = retTuple() // automatic unwrapping

```