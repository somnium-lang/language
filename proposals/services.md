

```Somnium

service Notifications {
  

  func put(channel: string, msg: string) {

  }
}


func main(args: [string]) {
  Notifications::put('test', 'Hello!')
}


// calling methods on the Worker service will schedule the call
// to an available worker

@service-instance-count(2)
async service Worker {
  func doWork(i: int) -> int {
    i * i
  }
}

```
