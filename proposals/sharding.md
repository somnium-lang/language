# Sharding



```Somnium

shard WorkerShard {

  class Worker {
    func work(a: int, b: int) -> int {
      a * b
    }
  }

}

async func main(args: [string]) {
  // Since the Worker class is declared inside the WorkerShard shard, all new
  // instances will be allocated inside the shard. Here `worker` is an async 
  // reference to the Worker allocated in the shard.
  let worker = Worker()

  // since worker is an async reference, all call to worker scheduled and returned as futures
  let result = await worker.work(2, 2)
}

```

TODO: add shard pool declaration and usage