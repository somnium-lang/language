```Somnium
let c = \x -> x * x // may or may not support this form, type would be generic <T>(T)->T
let r = c(2) // 4

let c: (int) -> int = \x -> x - 2
```