```Somnium

func add(a: int, b: int) -> int { a + b }
func mult(a: int, b: int) -> int { a * b }


let result = mult(add(4, 2), 2)
let result = 4 |> add(2) |> mult(2) // 12

```