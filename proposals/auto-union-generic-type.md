```Somnium

class Foo<T> {
  alloc() {}
  func bar(i: int) -> int {}
  func baz(t: T) -> int {}
}

func withFoo(f: Foo, i: int) {
  // Foo can be Foo<string> or Foo<int>
  // but both have the same impl of 'bar'
  // so we can use it here
  println("bar: {f.bar(i)}")

  let r = match f {
    f: Foo<string> -> f.baz('2')
    f: Foo<int> -> f.baz(2)
    _ -> 0
  }

  println("baz: {r}")
}

func main(args: [string]) {
  let a = Foo<string>()
  let b = Foo<int>()
  withFoo(a)
  withFoo(b)
}

```