
```Somnium
class Tree with OwnerOf<Leaf> {
  prop root: Leaf { get set }
  prop children: [Leaf] => root.children

  alloc(this.root) {}
}

// owned classes are shared
class Leaf ownedby Tree {
  prop parent: opt Leaf { get set }
  children = List<Leaf>()
}

func build(a: uint, b: uint) -> Tree {
  let mut tree = Tree #tree (
    Leaf() in #tree
  )

  // imperative style

  for i in 0...a {
    let mut la = Leaf() in tree
    la.parent := tree.root
    for n in 0...b {
      let mut lb = Leaf() in tree
      lb.parent := la
      la.children += lb
    }
    root.children += la
  }

  // functional style

  for i in 0...a {
    tree.root.children += Leaf #outer {
      parent: tree.root,
      children: for n in 0...b {
        Leaf {
          parent: #outer
        } in tree
      }
    } in tree
  }

  tree
}
```