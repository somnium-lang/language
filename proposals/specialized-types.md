```Somnium

type Name = typeof string

func checkName(s: Name) -> bool;

// --

let nameA = 'Bob'
let nameB: Name = 'Alice'

if checkName(nameA) {} // compile error, Name is a string but a string is not a Name
if checkName(nameA as Name) {} // OK
if checkName(nameB) {} // OK

```