```Somnium

let result = do {
  r <- getData()
  r <- r.filter(\x -> x.valid)
  r <- r.map(\x -> x.result)
  normalize(r)
}

```