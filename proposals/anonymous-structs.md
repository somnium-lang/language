```Somnium
// as generated type
let s = {
  x: 1,
  y: 2
}

let p = s.x * s.y

// as argument

struct Vec {
  x: int
  y: int
}

func foo(s: Vec) -> int

foo({
  x: 1,
  y: 2
})


```