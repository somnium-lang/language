
Properties are public by default.

```Somnium

class Foo {
  // property with default getter and setter initialized to bar
  prop property: string {get set} = 'bar'

  // read only property returning 22
  prop number: int {
    get {
      22
    }
  }

  // write only property
  _setting = 0
  prop setting: int {
    set i {
      _setting := i
    }
  }

  // r/w property
  _internal = 0
  prop external: int {
    get { _internal / 1000 }
    set i { _internal := i * 1000 }
  }

  // shorthand variant for read only property
  prop value: string => 'value'
}


let mut f = Foo {}

println(f.property) // prints: bar
f.property := 'baz' // sets property to baz
println(f.property) // prints: baz


```