```
type MonadError = typeof string

class Monad<A> {
  operator <-<B>(f: (A) -> B) -> Monad<B>

  operator match() -> MonadError
  operator match() -> A
}


func getMonad() -> Monad<int>

func main(args: [string]) {
  let m = getMonad()
  let t = m <- \i -> string::fromInt(i) ?? MonadError('invalid nr')
  match t {
    s: string -> s
    e: MonadError -> e
  }

  let t = m <- toString <- toNumber
}


let result = do monad {
  op1
  op2
  op3
} catch e {

}
```