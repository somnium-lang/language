```Somnium

struct Vector {
  x: int
  y: int
  z: int
}

func modX(v: Vector, i: int) -> Vector {
  v <- { x: v.x + i }
}

```