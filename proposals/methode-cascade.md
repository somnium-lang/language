

```Somnium

class Foo {
  _value = 0
  func mut add(i: int) {
    value += i
  }
  prop value: int => _value
}


let mut f = Foo {}

f..add(10)
 ..add(20)
 ..add(5)

println("f.value == {f.value}") // prints 35

```

with partial application

```Somnium

class Foo {
  _value = 0

  // add a + b to value and return value - b
  func mut addSum(a: int, b: int) -> int {
    value += a + b
    value - b
  }
  prop value: int => _value
}


let mut f = Foo {}

let i = f..add(10, 5) // value = 0 + 10 + 5 return 15 - 5 = 10
         ..add(20) // return value of previous cascade application is applied to resulting lambda (value = 15 + 20 + 10 return 45 - 10 = 35)
         ..add(5) // value = 45 + 5 + 35 return 85 - 35 = 50

println("f.value == {f.value} and i == {i}") // prints 35 and 50

```