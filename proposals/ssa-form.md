```Somnium
func doStuff() -> string;

func foo() {
  let x = 10
  // value of x is an integer with value '10'
  let x = doStuff(x) // redefinition of x, compiler will take care of the ssa form
  // the value of x will be a string here
}

```