

```Somnium

func foo(i: int, {bar: opt int, baz = 'default'}) {

}

func main(args: [string]) {
  foo(42, bar: 2, baz: 'Hello')
}

```