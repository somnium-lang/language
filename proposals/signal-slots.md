```Somnium

struct Product {
  name: string
  data: [byte]
}

service ProducerFactory : ThreadPoolFactory<Producer> {
  lastId = 0
  func create() -> Producer {
    return Producer(lastId++)
  }
}

class Producer {
  id: int

  alloc(this.id) {
    loop {
      produce()
    }
  }

  func produce() {
    let mut p = Product { name: 'Foo', data: [] }
    // work
    didProduce(id, p)
  }

  signal didProduce(producerId: int, p: Product)
}

class Consumer link Producer  {

  alloc() {}

  slot onDidProduce(producerId: int, p: Product) {
    println("Producer {producerId} produced {p.name}")
  }
}

func main(args: [string]) {
  let producers = ThreadPool::spawn<Producer>(ProducerFactory, 100)
  let consumer = Consumer()
}

```