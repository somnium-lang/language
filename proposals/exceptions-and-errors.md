# Error handling using errors and exceptions

When writing code, a developer will always have to deal with cases that deviate from the normal flow. Recognizing and dealing with these deviations is often the real challenge of writing good code. 

```Somnium

error Faulty()

exception FaultyRarely()

func getNumber() -> opt int

func faulty() -> int
throws Faulty {
  let i = getNumber() ?? throw Faulty()
}

func useFaulty() {
  let f = faulty()

  catch e: Faulty {
    handleError()
    // will exit scope/defer here
    // will return if directly inside a function scope
  }

  // normal flow
}

func x() {
  // using catch as an expression will return a value that is either
  // the result or the error
  let value: int | Faulty = catch faulty()
  
  // this can be combined with a match expression
  let value = match catch faulty() {
      v: int -> v
      e: Faulty -> -1
  }
}

func deferredFaulty() throws Faulty {
  let f = faulty()

  // we don't catch the error but we did indicate we may throw, error will be deferred

  // normal flow
}

func rarelyFaulty() -> int {
  if random() * 10 ** 6 == 123456 {
    throw FaultyRarely()
  }

  42
}

// exceptions can be caught globally

catch e: FaultyRarely {
  // somewhere this exception was thrown
}

func useRarely() {
  let i = rarelyFaulty()

  // we could catch here with catch e: FaultyRarely {}

  // normal flow
}


```

Errors must be caught and handled at the point they occur or included in the `throws` clause of the current function. Exceptions on the other hand 
Exceptions can be caught globally.

Errors are used for common occurrences that differ from the normal flow:
* doing IO operations
* handling user input

Exceptions are used for scenarios that are not impossible but very unlikely:
* memory allocation failures

```Somnium
pragma use-exceptions-as-errors
```