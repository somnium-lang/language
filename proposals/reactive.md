```Somnium

func test() {
  let mut a = 2
  let defer b = a * 2

  println("b: {b}") // b: 4
  a := 3
  println("b: {b}") // b: 6
}

```