```Somnium

trait NodeBase {
  func makeCopy() -> this
}

class SomeNode with NodeBase {
  alloc() {}
  
  func makeCopy() -> this {
    SomeNode()
  }
}

func withNodeBase(n: NodeBase) {
  let c = n.makeCopy() // type of c is NodeBase
}

func withSomeNode(n: SomeNode) {
  let c = n.makeCopy() // type of c is SomeNode
}

```