```Somnium

managed module app

// all declarations in this module will be managed

```

Allocations of managed classes and structs must be located in managed functions. These allocations will be managed by the garbage collector.
