```Somnium

module math

export func add(a: int, b: int) -> int { a + b }

module math::constants

export const phi: float = 1.61803398874989484820

```

``` Somnium

// import all exported members from module math
import math

func main(args: [string]) {
  println("1 + 1 = {add(1,1)}")
}

// to import a single member only (can be a module)

import math::{ add } // import only exported declaration add

// import math in new namespace Math

import math as Math

Math::add(1,1)

// import all modules which are direct descendants of `math`

import math::*

// import all modules under `math`

import math::**

```